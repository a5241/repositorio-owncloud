FROM php:7.4.25-apache
RUN apt-get update && apt-get install --no-install-recommends -y wget unzip zlib1g-dev libpng-dev libicu-dev libzip-dev && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    docker-php-ext-install zip gd intl && \
    wget https://download.owncloud.org/community/owncloud-complete-20210721.zip && \
    unzip owncloud-complete-20210721.zip && \
    mv owncloud/* /var/www/html



EXPOSE 80

